const findProject = function(project) {
  let urlString = window.location.href;
  let url = new URL(urlString);
  let projectName = url.searchParams.get("name");
  return project.name == projectName;
};

const createStatsLiElement = function(value, ul, label) {
  let div = createElement("div");
  div.innerHTML = label;
  let li = createElement("li");
  li.innerHTML = value;
  appendElement(div, li);
  appendElement(ul, div);
};

async function getContributorsData(projectFullName) {
  const url = "https://api.github.com/repos";
  const response = await fetch(`${url}/${projectFullName}/stats/contributors`);
  return response.json();
}

async function getCommitsData(projectFullName, page) {
  const url = "https://api.github.com/repos";
  const response = await fetch(
    `${url}/${projectFullName}/commits?page=${page}&per_page=5`
  );
  return response.json();
}

const createCommitDiv = function(ele) {
  let imgDiv = createElement("div");
  imgDiv.classList.add("imgDiv");

  let dataDiv = createElement("div");
  dataDiv.classList.add("dataDiv");
  let commitDiv = createElement("div");
  commitDiv.classList.add("commitDiv");
  let p = createElement("p");
  p.classList.add("authorName");
  if (ele.author == null) {
    p.innerHTML = "@" + ele.commit.author.name;
    imgDiv.classList.add("noAuthorImgDiv");
  } else {
    let img = createElement("img");
    img.src = ele.author.avatar_url;
    appendElement(imgDiv, img);
    p.innerHTML = "@" + ele.author.login;
  }
  let p3 = createElement("p");
  p3.innerHTML = ele.commit.author.date;
  let divDate = createElement("div");
  divDate.classList.add("commitDate");
  appendElement(divDate, p3);
  let p4 = createElement("p");
  p4.classList.add("commitMsg");
  p4.innerHTML = ele.commit.message;
  let dateNameDiv = createElement("div");
  dateNameDiv.classList.add("dateNameDiv");
  let nameDiv = createElement("div");
  appendElement(nameDiv, p);
  appendElement(dateNameDiv, p4);
  appendElement(dateNameDiv, divDate);
  appendElement(dataDiv, dateNameDiv);
  appendElement(dataDiv, nameDiv);
  appendElement(commitDiv, imgDiv);
  appendElement(commitDiv, dataDiv);
  appendElement(projectCommits, commitDiv);
};

apiData.then(projects => {
  let project = projects.find(findProject);
  console.log(project);
  let projectStats = document.getElementById("projectStats");
  let projectCommits = document.getElementById("projectCommits");
  let centerDivContent = document.getElementById("centerDivContent");
  const projectName = project.name;
  console.log(project.name);
  const projectFullName = project.full_name;
  createStatsLiElement(project.stargazers_count, projectStats, "stars");
  createStatsLiElement(project.forks_count, projectStats, "forks");
  const contribData = getContributorsData(projectFullName);
  contribData.then(project => {
    let numberCommits = 0;
    project.map(ele => {
      numberCommits = numberCommits + ele.total;
    });
    createStatsLiElement(project.length, projectStats, "contribs");
    let paginationLink = createElement("a");
    let pageNumber = 1;
    paginationLink.innerHTML = "Próximo";
    paginationLink.onclick = function() {
      if (pageNumber >= 0 && pageNumber < numberCommits - 1) {
        pageNumber++;
        document.getElementById("projectCommits").innerHTML = "";
        getCommitsData(projectFullName, pageNumber).then(commits => {
          commits.map(ele => {
            createCommitDiv(ele);
          });
        });
      }
    };
    let paginationLinkBack = createElement("a");
    paginationLinkBack.innerHTML = "Anterior";
    paginationLinkBack.onclick = function() {
      if (pageNumber >= 2) {
        pageNumber--;
        document.getElementById("projectCommits").innerHTML = "";
        getCommitsData(projectFullName, pageNumber).then(commits => {
          commits.map(ele => {
            createCommitDiv(ele);
          });
        });
      }
    };
    appendElement(centerDivContent, paginationLink);
    appendElement(centerDivContent, paginationLinkBack);
    getCommitsData(projectFullName, pageNumber).then(commits => {
      commits.map(ele => {
        createCommitDiv(ele);
      });
    });
  });
});
