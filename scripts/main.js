async function getGitHubApiData(userName) {
  const url = "https://api.github.com/users";
  const name = userName;
  const response = await fetch(`${url}/${name}/repos`);
  return response.json();
}

const createElement = function(tag) {
  return document.createElement(tag);
};

const appendElement = function(parent, child) {
  return parent.appendChild(child);
};

const apiData = getGitHubApiData("marvin-ai");
apiData.then(function(result) {
  let projectList = document.getElementById("projectList");
  let projectsStarsDesc = [];
  result.map((project, index) => {
    projectsStarsDesc[index] = project.stargazers_count;
  });
  projectsStarsDesc.sort(function(a, b) {
    return b - a;
  });
  let projectByStars = [];
  let cont = 0;
  result.map(project => {
    while (project.stargazers_count != projectsStarsDesc[cont]) {
      cont++;
    }
    projectByStars[cont] = project;
    cont = 0;
  });
  projectByStars.map(project => {
    let menuItem = createElement("li");
    let menuLink = createElement("a");
    menuLink.innerHTML = project.name;
    if (!window.location.href.includes("views")) {
      menuLink.href = `./views/projectDetails.html?name=${project.name}&page=1`;
    } else {
      menuLink.href = `./projectDetails.html?name=${project.name}`;
    }
    appendElement(menuItem, menuLink);
    appendElement(projectList, menuItem);
  });
});
